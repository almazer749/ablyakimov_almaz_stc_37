import java.util.Scanner;

public class Program3 {

    public static boolean isPrime(int sumOfDigits) {
        if (sumOfDigits == 2 || sumOfDigits == 3) {
            return true;
        }

        for (int i = 2; i * i <= sumOfDigits; i++) {
            if (sumOfDigits % i == 0) {
                return false;
            }
        }

        return true;
    }

    public static int getSumOfDigits(int currentNumber) {
        int sumOfDigits = 0;

        while (currentNumber != 0) {
            sumOfDigits += currentNumber % 10;
            currentNumber /= 10;
        }

        return sumOfDigits;
    }


    public static void main(String[] args) {
        int mulNumbers = 1;
        System.out.println("Enter numbers");
        for (int stop = 0; stop != 1; ) {
            Scanner scanner = new Scanner(System.in);

            int currentNumber = scanner.nextInt();
            if (currentNumber != 0) {
                if (isPrime(getSumOfDigits(currentNumber)) == true) {
                    mulNumbers *= currentNumber;
                }
            } else {
                stop = 1;
                System.out.println(mulNumbers);
            }

        }

    }
}
