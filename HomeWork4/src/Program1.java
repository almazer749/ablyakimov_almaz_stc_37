import java.util.Scanner;

public class Program1 {
    public static boolean powOfTwoChek(int n, int [] counter) {
        if (n % 2 == 0) {
            counter [0]++;
            return powOfTwoChek(n / 2, counter);
        } else {
            if (n == 1)
            {
                return true;
            }
            else{
                return false;
            }

        }
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int currentNumber = scanner.nextInt();
        int [] counter = {0};
        boolean checkNumber = powOfTwoChek(currentNumber, counter);
        System.out.println(checkNumber);
        if (checkNumber){
            System.out.println("Степень двойки равна " + counter [0]);
        }
    }
}
