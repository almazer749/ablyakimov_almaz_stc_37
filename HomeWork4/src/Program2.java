import java.util.Arrays;
import java.util.Scanner;

public class Program2 {
    public static boolean isExists(int[] array, int n) {
        for (int i = 0; i < array.length / 2; i++) {
            if (array[i] == n) {
                return true;
            } else {
                if (i == array.length / 2 - 1) {
                    return isExists(Arrays.copyOfRange(array, array.length / 2, array.length), n);
                }
            }
        }
        if(array.length == 1 && array[0] == n){
            return true;
        }
        else{
            return false;
        }
    }


    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int currentNumber = scanner.nextInt();
        int[] array = {2, 4, 74, 83, 87, 36, 12, 34, 45, 25, 90, 23, 76, 3, 21, 56};
        System.out.println(isExists(array, currentNumber));
    }
}