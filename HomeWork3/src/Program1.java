import java.util.Arrays;
import java.util.Scanner;

public class Program1 {
    public static int sumOfArray(int array[]) {
        int arraySum = 0;
        for (int j = 0; j < array.length; j++) {
            arraySum = arraySum + array[j];
        }
        return arraySum;
    }

    public static int[] revereseArray(int[] array) {
        for (int i = 0; i < array.length / 2; i++) {
            int temp = array[i];
            array[i] = array[array.length - 1 - i];
            array[array.length - 1 - i] = temp;
        }
        return array;
    }

    public static double averageOfArray(int[] array) {
        double arrayAverage = 0;
        if (array.length > 0) {
            double sum = 0;
            for (int j = 0; j < array.length; j++) {
                sum += array[j];
            }
            arrayAverage = sum / array.length;
        }
        return arrayAverage;
    }

    public static int[] swapMaxMin(int[] array) {
        int min = array[0];
        int max = array[0];
        int maxIndex = 0;
        int minIndex = 0;

        for (int i = 0; i < array.length; i++) {
            if (array[i] < min) {
                min = array[i];
                minIndex = i;
            }
            if (array[i] > max) {
                max = array[i];
                maxIndex = i;
            }
        }
        array[maxIndex] = min;
        array[minIndex] = max;
        return array;
    }

    public static int[] sortArray(int[] array) {
        boolean stop = false;
        int temp;
        while (!stop) {
            stop = true;
            for (int i = 0; i < array.length - 1; i++) {
                if (array[i] > array[i + 1]) {
                    stop = false;
                    temp = array[i];
                    array[i] = array[i + 1];
                    array[i + 1] = temp;
                }
            }

        }
        return array;
    }

    public static int findRank(int currentNumber) {
        int rank = 1;
        while (currentNumber / 10 != 0) {
            currentNumber /= 10;
            rank++;
        }
        return rank;
    }

    public static int digitsOfArray(int[] array) {
        int rank = 0;
        for (int i = array.length - 2; i >= 0; i--) {
            rank = findRank(array[i + 1]);
            array[i] *= Math.pow(10, rank);
        }
        return sumOfArray(array);
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        int array[] = new int[n];
        for (int i = 0; i < array.length; i++) {
            array[i] = scanner.nextInt();
        }
        int arraySum = sumOfArray(array);
        int tempArray[] = Arrays.copyOf(array, array.length);
        int tempArray2[] = Arrays.copyOf(array, array.length);
        System.out.println(arraySum);

        System.out.println(Arrays.toString(revereseArray(tempArray)));
        System.out.println(averageOfArray(array));
        System.out.println(Arrays.toString(swapMaxMin(array)));
        System.out.println(Arrays.toString(sortArray(array)));
        System.out.println(digitsOfArray(tempArray2));
    }
}
